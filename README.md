# Bank Account

Partendo dagli appunti del docente sulle strutture dati si realizzi quanto segue:
- la classe `Heap.java` con eventuali inner class ed iteratori,
- ogni studente opererà la consegna su di un branch personale.

## Valutazione

Oltre alla pulizia del codice, alla documentazione ed alle scelte operate il progetto deve essere consegnato in una cartella `Heap` che contenga il file `README.md` in cui si dà breve descrizione del progetto con attenzione alle scelte operate. Le valutazioni eccellenti (8, 9, 10) deriveranno da un consistente apporto personale e creativo.
